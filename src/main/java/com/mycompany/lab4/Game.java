/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.Scanner;

/**
 *
 * @author sirapolaya
 */
public class Game {

    private Table table;
    private Player player1, player2;

    public Game() {
        player1 = new Player('O');
        player2 = new Player('X');
    }
    
    public void play(){
        showWelcome();
        newGame();
        while(true){
            showTable();
            showTurn();
            inputRowCol();
            if (table.checkWin()){
                showTable();
                showInfo();
                newGame();
            }
            if(table.checkDraw()){
                showTable();
                showInfo();
                newGame();     
        }
        table.switchPlayer();
        
        }
    }
    
    private void showWelcome() {
        System.out.println("Welcome To XO Game");
    }

    private void showTable() {
        char[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j] + " ");
            }
            System.out.println();
        }
    }

    private void newGame() {
        table = new Table(player1, player2);
    }

    private void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Please Input Row Col : ");
        int row = kb.nextInt();
        int col = kb.nextInt();
        table.setRowCol(row, col);
        //table.switchPlayer();

    }

    private void showTurn() {
        System.out.println(table.getCurrentPlayer() + " Turn");
    }
    
    private void showInfo() {
        System.out.println(player1);
        System.out.println(player2);
    }

}
